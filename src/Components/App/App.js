import React, { Component } from 'react';
import MarketmanApp from "./MarketmanApp.js";
import './App.css';


class App extends Component {
  render() {
  return (
    <div>
	    <MarketmanApp />
    </div>
  );
 }
}

export default App;
