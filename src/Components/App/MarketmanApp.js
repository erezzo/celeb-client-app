import React, { Component } from "react";
import './App.css';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { Button, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import moment from "moment";
import axios from "axios";

const API_BASE = "http://localhost:56573/";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
    maxWidth: 700,
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  },
}));

class MarketmanApp extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      celebs: []
    };
  }

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    axios.get(API_BASE + `api/celebs`).then(response => {
        
        this.setState({
            celebs: response.data
          });
        
      }).catch((err) => {console.log(err)});
  }  

  deleteCeleb(item){
    
    let updatedList = this.state.celebs.filter( el => el.id !== item.id ); 
    this.setState({
        celebs: updatedList
      });
    axios.delete(API_BASE + `api/celebs/`+ item.id).then(response => {
        
        this.loadData();
      }).catch((err) => {console.log(err)});

  }
  resetData(){
      alert("Reload data may take few min. your page will be updated when finish loading.");
    axios.post(API_BASE + `api/celebs/reset`).then(response => {
        this.setState({
            celebs: response.data
          });
      }).catch((err) => {console.log(err)});

  }
 genderToString(gender){
    switch(gender) {
      case 1:
        return 'Male'
      case 2:
        return 'Female'
      default:
        return 'Unspecified'
    }
  };

  render() {
    var i = 0;
    return (
      <Container  className="App">
        <Paper>
        
          <Typography variant="h4" component="h1" gutterBottom>
            MarketMan Celebrities App
          </Typography>
          <Button variant="contained" size="small" color="primary" onClick={() => this.resetData()} style={{width:"45%"}}>
                  Reset Data
                </Button>
          <GridList cellHeight={450} className={useStyles.gridList} cols={5}>
            {this.state.celebs.map((tile) => (                
              <Card className={useStyles.root} key={i++} >
              <CardActionArea style={{height:"75%"}}>            
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    {!!(tile.name) ? tile.name : ""}
                  </Typography>                
                  <Typography variant="body2" color="textSecondary" component="p">
                    {!!(tile.birthDate) ? moment(tile.birthDate).format("MMMM Do[,] YYYY") : ""}                  
                  </Typography>
                  
                  <img className={useStyles.img} src={tile.imagePath} alt={tile.name} style={{width:"50%", height:"50%"}}/>
                  
                  <Typography gutterBottom variant="subtitle2" component="p">
                    {!!(tile.jobTitle) ? tile.jobTitle.join(", ") : ""}
                  </Typography>
                  <Typography gutterBottom variant="subtitle2" component="p">
                    Gender: {this.genderToString(tile.gender)}
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions style={{height:"25%"}}>
                <Button variant="outlined" size="small" color="primary" onClick={() => this.deleteCeleb(tile)} style={{flex:"auto"}}>
                  Delete
                </Button>
              </CardActions>
            </Card>
            ))}
          </GridList>
          
        </Paper>
      </Container>
    );
  }
}

export default MarketmanApp;
